//find average of n numbers without array
#include<stdio.h>
int main()
{
    int a[20],sum=0; 
    float avg=0.0,n;
    printf("Enter the value of n: ");
    scanf("%f",&n);
    for(int i=1;i<=n;i++)
    {   
        sum=sum+i;
    }
    avg=sum/n;
    printf("The average is %f. \n",avg); 
    return 0;
}